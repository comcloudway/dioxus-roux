use std::collections::HashMap;

/// represents a path
/// you probably want to join the vec using the / character
/// but it is up to you really
pub type Route = Vec<String>;

/// compare a template(target) Route to the Route the user is currently on
/// to use variables/placeholders you can prefix the name with a :
/// if you want to create a wildcard use the * symbol
/// NOTE: if the algorithm encounters a * it will stop parsing and jump to the end
/// if you only want to use the wildcard for one part, consider using a variable
pub fn compare(target: &Route, value: &Route) -> Option<HashMap<String, String>> {
    log::info!("Comparing {:?} to {:?}", target, value);
    if target == value {
        Some(HashMap::new())
    } else {
        let mut index = 0;
        let mut is_wildcard = false;
        let mut is_same = true;
        let mut map = HashMap::new();

        while is_same && !is_wildcard {
            if let Some(template) = target.get(index) {
                if template==&String::from('*') {
                    // no need to compare is wildcard
                    is_wildcard = true;
                } else if let Some(value) = value.get(index) {
                    if value == template {
                        // normal value
                    } else if template.starts_with(':') {
                        // id
                        let key = template.strip_prefix(':').unwrap_or(template);
                        map.insert(key.to_string(), value.to_string());
                    } else {
                        // no match x -> not same
                        is_same = false;
                    }
                } else {
                    // no wildcard, but also no value -> not same
                    is_same = false;
                }
            } else {
                // mark end of comparison
                is_wildcard = true;
            }
            index+=1;
        }

        if is_same {
            log::info!("compare: found match {:?}", map.clone());
            Some(map)
        } else {
            log::info!("compare: found no match");
            None
        }
    }
}
