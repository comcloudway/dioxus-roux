mod dummy;
pub use dummy::dummy_backend;

#[cfg(feature = "web")]
mod web;
#[cfg(feature = "web")]
pub use web::web_hash_backend;
