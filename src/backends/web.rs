use std::{
    sync::Arc,
    cell::RefCell
};
use futures_channel::mpsc::{
    unbounded as channel,
};
use futures_util::StreamExt;
use web_sys;
use gloo_events::EventListener;

use crate::Roux;

/// a hash based router
pub async fn web_hash_backend(
    roux: Arc<RefCell<Roux>>,
    path_seperator: char
) {
    log::info!("web_hash_backend booting");
    let (tx, mut rx) = channel();
    roux.borrow_mut().register_onchange_listener(tx);

    // core->backend
        if let Ok(h) = web_sys::window().unwrap().location().hash() {
            log::info!("web_hash_backend: new path {}", h);
            roux.borrow_mut().next(
                h.strip_prefix('#').unwrap_or(&h).split(path_seperator)
                                                 .map(|d| d.to_string())
                                                 .collect()
            );
        }
        let _listener = EventListener::new(
            &web_sys::window().unwrap(),
            "hashchange",
            move |_| {
                if let Ok(h) = web_sys::window().unwrap().location().hash() {
                    log::info!("web_hash_backend: new path {}", h);
                    roux.borrow_mut().next(
                        h.strip_prefix('#').unwrap_or(&h).split(path_seperator)
                                                         .map(|d| d.to_string())
                                                         .collect()
                    );
                }
            }
        );
    log::info!("web_hash_backend registered");
        while let Some(msg) = rx.next().await {
            log::info!("web_hash_backend: received msg {:?}", msg);
            web_sys::window().unwrap().location().set_hash(&msg.join(&path_seperator.to_string()));
    };
}
