use std::{
    sync::Arc,
    cell::RefCell
};
use futures_channel::mpsc::{
    unbounded as channel,
};
use futures_util::StreamExt;

use crate::Roux;

/// a hash based router
pub async fn dummy_backend(
    roux: Arc<RefCell<Roux>>,
) {
    log::info!("dummy_backend booting");
    let (tx, mut rx) = channel();
    roux.borrow_mut().register_onchange_listener(tx);

    log::info!("dummy_backend cant detect path changes");

    log::info!("dummy_backend registered");
        while let Some(msg) = rx.next().await {
            log::info!("dummy_backend: received msg {:?}", msg);
    };
}
