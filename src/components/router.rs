use std::{
    sync::Arc,
    cell::RefCell,
};
use dioxus::prelude::*;
use crate::{
    Roux,
    RouxContext,
};

#[inline_props]
#[allow(non_snake_case)]
pub fn Router<'a>(
    cx: Scope<'a>,
    children: Element<'a>,
    backends: Option<Arc<dyn Fn(RouxContext)>>
) -> Element<'a>
{
    cx.use_hook(|_| {
        let roux = Roux::new();
        let roux = Arc::new(RefCell::new(roux));

        if let Some(backends) = backends {
            cx.spawn({
                let roux = roux.clone();
                let backends = backends.clone();
                async move {
                    backends(roux.clone());
                }});
        }

        cx.provide_context::<RouxContext>(roux);
    });

    cx.render(rsx!{
        children
    })
}
