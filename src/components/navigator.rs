use dioxus::prelude::*;
use crate::{
    RouxContext,
    utils
};

#[inline_props]
#[allow(non_snake_case)]
pub fn NavigatorButton<'a>(
    cx: Scope<'a>,
    text: &'a str,
    to: utils::Route
) -> Element<'a> {
    let roux = cx.consume_context::<RouxContext>();

    cx.render(rsx!{
        button {
            onclick: move |_| {
                if let Some(roux) = roux.clone() {
                    log::info!("Found roux ctx - navigating to {:?}", to.clone());
                    roux.borrow_mut().next(to.clone());
                }
            },
            "{text}"
        }
    })
}
