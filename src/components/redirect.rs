use std::collections::HashMap;
use dioxus::prelude::*;
use futures_channel::mpsc::{
    unbounded as channel,
    UnboundedReceiver as Receiver,
};
use futures_util::StreamExt;

use crate::{
    RouxContext,
    utils::{
        compare as comp,
        self
    }
};

#[inline_props]
#[allow(non_snake_case)]
pub fn Redirect(
    cx: Scope,
    target: utils::Route,
    to: utils::Route,
) -> Element {
    let core = cx.consume_context::<RouxContext>();

    let _ka = use_coroutine(&cx, |_: Receiver<usize>| {
        let core = core.clone();
        let to = to.clone();
        let target = target.clone();

        async move {
            if let Some(core) = core {
                let path = core.borrow().current();

                // check if url is supposed to be redirected
                // if true, replace possible variables wih their value
                if let Some(results) = comp(&target, &path) {
                    core.borrow_mut().replace(
                        to.clone()
                          .iter()
                          .map(|p| {
                              if let Some(variable) = p.strip_prefix(':') {
                                  if let Some(value) = results.get(variable) {
                                      return value.to_string();
                                  }
                              }

                              p.to_string()
                          })
                          .collect()
                    );
                }

                let (tx, mut rx) = channel();
                core.borrow_mut().register_onchange_listener(tx);
                while let Some(msg) = rx.next().await {
                    // check if url is supposed to be redirected
                    // if true, replace possible variables wih their value
                    if let Some(results) = comp(&target, &msg ) {
                        core.borrow_mut().replace(
                            to.clone()
                              .iter()
                              .map(|p| {
                                  if let Some(variable) = p.strip_prefix(':') {
                                      if let Some(value) = results.get(variable) {
                                          return value.to_string();
                                      }
                                  }

                                  p.to_string()
                              })
                              .collect()
                        );
                    }
                };
            }
        }
    });

    // always renders nothing
    None
}
