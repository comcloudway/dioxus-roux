use std::collections::HashMap;
use dioxus::prelude::*;
use futures_channel::mpsc::{
    unbounded as channel,
    UnboundedReceiver as Receiver,
};
use futures_util::StreamExt;

use crate::{
    RouxContext,
    RouteContext,
    utils::{
        compare as comp,
        self
    }
};

#[inline_props]
#[allow(non_snake_case)]
pub fn Route<'a>(
    cx: Scope<'a>,
    target: utils::Route,
    children: Element<'a>
) -> Element<'a> {
    let core = cx.consume_context::<RouxContext>();
    let current = use_state(&cx, Vec::new);
    let render_me = use_state(&cx, || false);
    let needs_refresh = use_ref(&cx, ||true);

    let mut rctx = cx.provide_context(RouteContext {
        target: target.clone(),
        path: None,
        props: HashMap::new()
    });

    if *needs_refresh.read() {
        needs_refresh.set(false);
        if let Some(results) = comp(target, current) {
            rctx.path = Some(current.to_vec());
            rctx.props=results;
            render_me.set(true);
        }  else {
            render_me.set(false);
            rctx.path = None;
            rctx.props = HashMap::new();
        }
    }

    let _ka = use_coroutine(&cx, |_: Receiver<usize>| {
        let current = current.clone();
        let needs_refresh = needs_refresh.clone();
        let core = core.clone();

        async move {
            if let Some(core) = core {
                let path = core.borrow().current();
                current.set(path);
                needs_refresh.set(true);


                let (tx, mut rx) = channel();
                core.borrow_mut().register_onchange_listener(tx);
                while let Some(msg) = rx.next().await {
                    current.set(msg);
                    needs_refresh.set(true);
                };
            }
        }
    });

    if **render_me {
        return cx.render(rsx!{
            children
        });
    }

    None
}
