use std::{
    collections::HashMap,
    sync::Arc,
    cell::RefCell
};
use futures_channel::mpsc::{
    UnboundedSender as Sender
};

use crate::utils::Route;

#[derive(Clone)]
/// router core
pub struct Roux {
    /// stores the history
    history: Vec<Route>,
    /// collection of channel senders
    /// when the current route changes, every channel should be notified
    onchange: Vec<Sender<Route>>,
}
impl Roux {
    /// initialize a new roux core
    pub fn new() -> Self {
        Self {
            history: Default::default(),
            onchange: Vec::new()
        }
    }

    /// navigates back in the history
    /// returns the old route
    pub fn back(&mut self) -> Option<Route> {
        let r = self.history.pop();

        self.notify();

        r
    }
    /// adds a new path to the history
    pub fn next(&mut self, route: Route) {
        self.history.push(route);

        self.notify();
    }
    /// replaces the current path with a new one
    /// useful for pages, where the user should not be able to go back
    pub fn replace(&mut self, route: Route) {
        if let Some(item) = self.history.last_mut() {
            *item = route;
        }

        self.notify();
    }

    /// returns the current path
    /// defaults to an empty vector
    pub fn current(&self) -> Route {
        if let Some(r) = self.history.last() {
            r.clone()
        } else {
            Vec::new()
        }
    }

    /// notifies all backends
    pub fn notify(&self) {
        let current = if let Some(current) = self.history.last() {
            current.clone()
        } else { Vec::new() };

        log::info!("Notifying {} listener(s) with {:?}", self.onchange.len(), current);
        self.onchange.iter().for_each(|i| {
            i.clone().unbounded_send(current.clone());
        });
    }

    /// registers a new channel sender
    /// for the notify function
    /// used to receive path updates
    /// can be seen as a way to link backends to the core
    pub fn register_onchange_listener(&mut self, tx: Sender<Route>) {
        self.onchange.push(tx);
        log::info!("Registered new listener ({})", self.onchange.len());
    }
}

/// shortcut for Roux provide context type
pub type RouxContext = Arc<RefCell<Roux>>;

/// stores data about the current route
/// to be used by route children
#[derive(Clone)]
pub struct RouteContext {
    /// the path currently displayed in the front end
    pub path: Option<Route>,
    /// the target(template) path
    /// that was used to match this path
    pub target: Route,
    /// variables detected by compare()
    pub props: HashMap<String, String>
}
