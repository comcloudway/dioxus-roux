#![feature(async_closure, future_join, future_poll_fn)]

use dioxus::prelude::*;
use dioxus_roux::{
    Router,
    Route,
    Redirect,
    NavigatorButton,

    RouxContext,

    backends::{
        web_hash_backend,
        dummy_backend
    },

    roux,
};
use std::{
    sync::Arc,
    future::join
};
use async_std::task;

fn main() {
    wasm_logger::init(wasm_logger::Config::new(log::Level::Debug));
    dioxus::web::launch(app);
}
fn app(cx: Scope) -> Element {
    cx.render(rsx! (
        div { "Roux Example" }

        Router {
            backends: Arc::new(move |r| {
                task::block_on(async {
                    log::info!("Beginning backend registration");

                    join!(
                        web_hash_backend(r.clone(), '/'),
                        dummy_backend(r),
                    ).await;

                    log::info!("Finished backend registration");
                });
            }),

            NavigatorButton {
                text: "Go to World",
                to: roux!["world"]
            }
            NavigatorButton {
                text: "Text redirect by going to /user/world",
                to: roux!["user", "world"]
            }
            NavigatorButton {
                text: "Go home",
                to: roux![]
            }

            Route {
                target: roux!["world"],

                div {
                    "Hello World"
                }
            }

            Redirect {
                target: roux!["user", ":id"],
                to: roux![":id"]
            }
        }
    ))
}
